/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.30 : Database - wenjuan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wenjuan` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wenjuan`;

/*Table structure for table `answer` */

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `question_type` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `answer_content` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `answer` */

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `question_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `content` varchar(64) NOT NULL,
  PRIMARY KEY (`question_id`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `options` */

insert  into `options`(`question_id`,`sequence`,`content`) values (36,1,'3312'),(36,2,'313'),(37,1,'321'),(37,2,'322'),(39,1,'321'),(39,2,'321'),(40,1,'321'),(40,2,'12'),(41,1,'32'),(41,2,'321'),(42,1,'132'),(42,2,'32');

/*Table structure for table `paper` */

DROP TABLE IF EXISTS `paper`;

CREATE TABLE `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` varchar(64) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `paper` */

insert  into `paper`(`id`,`user_id`,`title`,`description`,`start_time`,`end_time`,`status`) values (8,1,'测试','测试',NULL,NULL,'START');

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `sequence` double DEFAULT NULL,
  `title_type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `question` */

insert  into `question`(`id`,`paper_id`,`type`,`title`,`sequence`,`title_type`) values (41,8,1,'123',3,0),(42,8,1,'1655728814446.mp4',2,1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `type` int(1) DEFAULT NULL COMMENT '1用户 2管理员',
  `phone_num` varchar(255) DEFAULT NULL COMMENT '手机',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`password`,`type`,`phone_num`,`email`) values (1,'adad','123456',2,'13355555555','admin@qq.com'),(25,'123456','123123',1,'12312313222','eqwrewr@qq.com'),(27,'1234567','123123',1,'12344321233','13241243@qq.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
