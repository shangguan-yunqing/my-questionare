package com.computer.network.utils;

import com.computer.network.vo.ResponseVO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * CSV工具类
 */
public class ExportUtils {
    private final static String NEW_LINE = "\n";
    private final static String CONNECT_ERROR="连接失败";

    /**
     * 生成为发送而准备的csv文件
     * @param fileName 文件名
     * @param head 表头
     * @param values 数据
     * @return
     * @throws IOException
     */
    public static File makeTempCSV(String fileName, String[] head, List<String[]> values) throws IOException {
        //准备文件
        File file = File.createTempFile(fileName,".csv");
        System.out.println(file.toURI());
        CSVFormat formator = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE);

        //准备写入
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"));
        CSVPrinter printer = new CSVPrinter(bufferedWriter,formator);

        //写入数据
        printer.printRecord(head);
        for(String[] val:values){
            printer.printRecord(val);
        }
        printer.close();
        bufferedWriter.close();
        //返回文件
        return file;
    }

    /**
     * 将csv文件发送
     * @param response
     * @param file
     * @return
     */
    public static ResponseVO sendFile(HttpServletResponse response,File file,String type) {
        //准备响应数据
        response.reset();
        switch (type) {
            case "csv":
                response.setContentType("text/csv");
                break;
            case "xlsx":
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                break;
        }
        response.setCharacterEncoding("utf-8");
        response.setContentLengthLong(file.length());
        response.setHeader("Content-Disposition", "attachment;filename=" + file.getName());
        //传输
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            byte[] buff = new byte[1024];
            OutputStream os = response.getOutputStream();
            int i = 0;
            while ((i = bis.read(buff)) != -1) {
                os.write(buff, 0, i);
                os.flush();
            }
            return ResponseVO.buildSuccess();
        } catch (IOException e) {
            return ResponseVO.buildFailure(CONNECT_ERROR);
        }
    }
    public static File makeTempXlsx(String fileName,List<List<String>> data) throws IOException {
        File file = File.createTempFile(fileName,".xlsx");
        SXSSFWorkbook workbook;

        workbook = new SXSSFWorkbook();
        System.out.println("new workbook");
        Sheet sheet = workbook.createSheet("原始数据");

        for(int i=0;i<data.get(0).size();i++){
            Row row = sheet.createRow(i);
            for(int j=0;j<data.size();j++){
                Cell cell = row.createCell(j);
                cell.setCellValue(data.get(j).get(i));
            }
        }
        OutputStream outputStream;
        outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
        return file;
    }

}
