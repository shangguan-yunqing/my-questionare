package com.computer.network.controller;

import com.computer.network.service.PaperService;
import com.computer.network.vo.PaperVO;
import com.computer.network.vo.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/paper")
public class PaperController {
    @Autowired
    PaperService paperService;

    /**
     * 添加问卷信息
     * @param paperVO 问卷
     * @return 调用状态
     */
    @PostMapping("/addPaper")
    public ResponseVO addPaper(@RequestBody PaperVO paperVO){
        return paperService.addPaper(paperVO);
    }

    /**
     * 更新问卷基本信息
     * @param paperVO 问卷
     * @return 调用状态
     */
    @PostMapping("/updatePaper")
    public ResponseVO updatePaper(@RequestBody PaperVO paperVO){
        return paperService.updatePaper(paperVO);
    }

    /**
     * 根据问卷ID删除问卷及其关联数据
     * @param paperId 问卷Id
     * @return 调用状态
     */
    @GetMapping("/{paperId}/deletePaper")
    public ResponseVO invalidatePaper(@PathVariable Integer paperId){
        return paperService.deletePaper(paperId);
    }

    /**
     * 根据用户ID获取某个用户创建的全部问卷
     * @param userId 用户ID
     * @return 调用状态（如果成功，则包含该用户创建的问卷列表）
     */
    @GetMapping("/{userId}/getUserPapers")
    public ResponseVO getUserPapers(@PathVariable Integer userId){
        return paperService.getUserPapers(userId);
    }

    /**
     * 根据问卷ID获取问卷详细信息
     * @param paperId 问卷ID
     * @return 调用状态（如果成功，则包含该问卷的详情信息）
     */
    @GetMapping("/{paperId}/checkPaper")
    public ResponseVO checkPaper(@PathVariable Integer paperId){
        return paperService.checkPaper(paperId);
    }

    /**
     *根据问卷ID统计该问卷的回答情况
     * @param paperId 问卷ID
     * @return 调用状态（如果成功，包含该问卷每个问题的回答情况，选择题包含每个选项被选择的次数，简答题包含题目的所有回答
     */
    @GetMapping("/{paperId}/reviewPaper")
    public ResponseVO reviewPaper(@PathVariable Integer paperId){
        return paperService.reviewPaper(paperId);
    }
    @GetMapping("/{paperId}/export")
    public ResponseVO exportPaper(String type,HttpServletResponse response, @PathVariable Integer paperId) { return paperService.exportPaper(type,response,paperId); }
}
