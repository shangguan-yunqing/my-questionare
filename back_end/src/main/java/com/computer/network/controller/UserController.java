package com.computer.network.controller;

import com.computer.network.service.UserService;
import com.computer.network.vo.ResponseVO;
import com.computer.network.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;

    /**
     * 添加用户
     * @param userVO 用户
     * @return 调用状态
     */
    @PostMapping("/register")
    public ResponseVO addUser(@RequestBody UserVO userVO){
        return userService.addUser(userVO);
    }

    /**
     * 用户登录账户检测
     * @param userVO 用户信息
     * @return 调用状态
     */
    @PostMapping("/login")
    public ResponseVO login(@RequestBody UserVO userVO){
        return userService.login(userVO);
    }

    /**
     * 获取普通用户列表
     * @return 调用状态（如果成功，则附带用户列表）
     */
    @GetMapping("/list")
    public ResponseVO list(){
        return userService.list();
    }

    /**
     * 根据用户ID删除用户及其关联问卷
     * @param id 用户ID
     * @return 调用状态
     */
    @PostMapping("/deleteById/{id}")
    public ResponseVO deleteById(@PathVariable int id){
        return userService.deleteById(id);
    }

    /**
     * 根据用户ID获取用户信息
     * @param id 用户ID
     * @return 调用状态（如果成功，则附带用户信息）
     */
    @GetMapping("/get/{id}")
    public ResponseVO get(@PathVariable int id){
        return userService.get(id);
    }

}
