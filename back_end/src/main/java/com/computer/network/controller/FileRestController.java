package com.computer.network.controller;

import com.computer.network.vo.ResponseVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/api/file")
public class FileRestController {

    @PostMapping("/upload")
    public ResponseVO videoUpload(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            String fileName = saveFile(file);
            return ResponseVO.buildSuccess(fileName);
        }
        return ResponseVO.buildFailure("视频不能为空");
    }

    /**
     *
     * @param multipartFile
     * @return 返回保存的路径
     */
    public String saveFile(MultipartFile multipartFile) {
        String fileName = "";
        try {
            String filePath = System.getProperty("user.dir") + File.separator + "video" +  File.separator;
            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdirs();
            }

            fileName = "请观看视频并回答视频中的问题" + getFileExtension(multipartFile.getOriginalFilename());

            multipartFile.transferTo(new File(filePath, fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }

    private static String getFileExtension(String fileName) {
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf("."));
        else return " ";
    }

}
