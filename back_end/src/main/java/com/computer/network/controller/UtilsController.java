package com.computer.network.controller;

import com.computer.network.mapper.PaperMapper;
import com.computer.network.mapper.UserMapper;
import com.computer.network.vo.PaperVO;
import com.computer.network.vo.UserVO;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@RestController
@RequestMapping("/api/utils")
public class UtilsController {
    @Autowired
    UserMapper userMapper;

    @Autowired
    PaperMapper paperMapper;


    /**
     * 将用户信息导出为excel表格
     */
    @GetMapping("/userinfo")
    @CrossOrigin(value = "http://localhost:8080",allowedHeaders = "Access-Control-Allow-Origin")
    public void downAll(HttpServletResponse response) throws Exception {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("基本信息");
        List<UserVO> list = userMapper.getUserList();
        //获取当前项目工作路径，并创建存放excel表格的文件夹
        String filePath = System.getProperty("user.dir") + File.separator + "excel" +  File.separator;
        //设置要导出的文件的名字
        String fileName = "userList" + ".xls";

        File file =new File(filePath);
        if(!file.exists()){
            file.mkdir();
        }

        //定义表头数据
        String[] headers = {"用户id", "用户名","用户类型","电话","邮箱"};
        //headers表示excel表中第一行的表头
        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头
        for (int i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }
        UserVO user = null;
        //在表中存放查询到的数据放入对应的列
        for (int i = 0; i < list.size(); i++) {
            user = list.get(i);
            HSSFRow row1 = sheet.createRow(i + 1);
            row1.createCell(0).setCellValue(user.getId());//写ID
            row1.createCell(1).setCellValue(user.getName());//写用户名
            row1.createCell(2).setCellValue(user.getType());//写类型
            row1.createCell(3).setCellValue(user.getPhoneNum());//写电话
            row1.createCell(4).setCellValue(user.getEmail());//写邮箱
        }
        file = new File(filePath+fileName);
        workbook.write(file);

        workbook.close();

        response.reset();
        response.setContentType("text/.xls");
        response.setCharacterEncoding("utf-8");
        response.setContentLengthLong(file.length());
        response.setHeader("Content-Disposition","attachment;filename="+file.getName());
        System.out.println(file.getName());
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            byte[] buff = new byte[1024];
            OutputStream os = response.getOutputStream();
            int i = 0;
            while ((i = bis.read(buff)) != -1) {
                os.write(buff, 0, i);
                os.flush();
            }


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @GetMapping("/{userId}/questionnaireList")
    public void questionnaireList(HttpServletResponse response,@PathVariable Integer userId) throws IOException {
        List<PaperVO> list=paperMapper.getUserPapers(userId);
        UserVO userVO = userMapper.getById(userId);
        String userName=userVO.getName();
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("问卷列表");
        //获取当前项目工作路径，并创建存放excel表格的文件夹
        String filePath = System.getProperty("user.dir") + File.separator + "excel" +  File.separator;
        //设置要导出的文件的名字
        String fileName = "user" + userId+"PaperList.xls";

        File file =new File(filePath);
        if(!file.exists()){
            file.mkdir();
        }
        //定义表头数据
        String[] headers = {"用户id", "用户名","问卷Id","问卷标题","问卷描述"};
        //headers表示excel表中第一行的表头
        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头
        for (int i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }
        PaperVO paperVO = null;
        //在表中存放查询到的数据放入对应的列
        for (int i = 0; i < list.size(); i++) {
            paperVO = list.get(i);
            HSSFRow row1 = sheet.createRow(i + 1);
            row1.createCell(0).setCellValue(userId);//写用户ID
            row1.createCell(1).setCellValue(userName);//写用户名
            row1.createCell(2).setCellValue(paperVO.getId());//写问卷ID
            row1.createCell(3).setCellValue(paperVO.getTitle());//写问卷标题
            row1.createCell(4).setCellValue(paperVO.getDescription());//写问卷描述
        }
        file = new File(filePath+fileName);
        workbook.write(file);
        workbook.close();
        response.reset();
        response.setContentType("text/.xls");
        response.setCharacterEncoding("utf-8");
        response.setContentLengthLong(file.length());
        response.setHeader("Content-Disposition","attachment;filename="+file.getName());
        System.out.println(file.getName());
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            byte[] buff = new byte[1024];
            OutputStream os = response.getOutputStream();
            int i = 0;
            while ((i = bis.read(buff)) != -1) {
                os.write(buff, 0, i);
                os.flush();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
