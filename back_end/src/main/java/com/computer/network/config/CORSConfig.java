package com.computer.network.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CORSConfig {
    /**
     配置允许跨域访问的ip--从这些url发过来的请求，都是允许支持的跨源请求
     */
    private static String[] originsVal = new String[]{
            "localhost:8081",
            "localhost:8099",
            "localhost:8080",
            "127.0.0.1:8080",
            "39.97.124.144:8080",
            "127.0.0.1",
            "localhost",
    };

    /**
     * 项目加载时，把过滤器生成，来统一管理跨源请求（不用再在每个controller上单独配置）
     * @return
     */
    @Bean
    public CorsFilter corsFilter() {
        //配置跨域访问的过滤器
        //基于url的数据源
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        addAllowedOrigins(corsConfiguration); //把允许的跨域源添加到corsConfiguration中
        corsConfiguration.addAllowedHeader("*"); // 允许所有头部
        corsConfiguration.addAllowedMethod("*"); // 允许所有方法
        corsConfiguration.setAllowCredentials(true); //允许跨域访问(在响应报文里带上跨域请求的凭证，和浏览器请求里面xhrFields相匹配，前后端才能正常通信)
        source.registerCorsConfiguration("/**", corsConfiguration); //指定对当前这个服务下的所有请求都启用corsConfiguration的配置
        return new CorsFilter(source);
    }

    /**
     * 添加允许的跨域源
     * @param corsConfiguration
     */
    private void addAllowedOrigins(CorsConfiguration corsConfiguration) {
        for (String origin : originsVal) {
            corsConfiguration.addAllowedOrigin("http://" + origin);
            corsConfiguration.addAllowedOrigin("https://" + origin);
        }
    }
}