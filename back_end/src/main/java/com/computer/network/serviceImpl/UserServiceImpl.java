package com.computer.network.serviceImpl;

import com.computer.network.mapper.AnswerMapper;
import com.computer.network.mapper.UserMapper;
import com.computer.network.mapper.PaperMapper;
import com.computer.network.service.UserService;
import com.computer.network.mapper.AnswerMapper;
import com.computer.network.vo.PaperVO;

import com.computer.network.vo.ResponseVO;
import com.computer.network.service.PaperService;
import com.computer.network.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final static String ACCOUNT_EXIST = "账号已存在";
    private final static String ACCOUNT_INFO_ERROR = "用户名或密码错误";
    private final static String UNKNOWN_ERROR = "未知错误";
    @Autowired
    UserMapper userMapper;

    @Autowired
    PaperService paperService;

    @Autowired
    PaperMapper paperMapper;

    @Autowired
    AnswerMapper answerMapper;

    /**
     * 添加用户
     * @param userVO 用户
     * @return 调用状态
     */
  @Override
    public ResponseVO addUser(UserVO userVO) {
        try {
            if(userMapper.getUserByName(userVO.getName(), userVO.getType())!=null){
                return ResponseVO.buildFailure("用户名已存在");
            }
            if(userVO.getId() == null){
                userMapper.addUser(userVO);
            }else{
                userMapper.setUser(userVO);
            }

            return ResponseVO.buildSuccess();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseVO.buildFailure(UNKNOWN_ERROR);
        }
    }

    /**
     * 用户登录账户检测
     * @param userVO 用户信息
     * @return 调用状态
     */
     @Override
    public ResponseVO login(UserVO userVO) {
        UserVO foundUser=userMapper.getUserByName(userVO.getName(),userVO.getType());
        if(foundUser==null || !foundUser.getPassword().equals(userVO.getPassword()))
            return ResponseVO.buildFailure(ACCOUNT_INFO_ERROR);
        return ResponseVO.buildSuccess(foundUser);
    }

    /**
     * 获取普通用户列表
     * @return 调用状态（如果成功，则附带用户列表）
     */
   @Override
    public ResponseVO list() {
        List<UserVO> list = userMapper.getUserList();
        return ResponseVO.buildSuccess(list);
    }

    /**
     * 根据用户ID获取用户信息
     * @param id 用户ID
     * @return 调用状态（如果成功，则附带用户信息）
     */
    @Override
    public ResponseVO get(int id) {
       return ResponseVO.buildSuccess(userMapper.getById(id));
    }

    /**
     * 根据用户ID删除用户
     * @param id 用户ID
     * @return 调用状态
     */
    @Override
    public ResponseVO deleteById(int id) {
        //根据用户ID在数据库中查找该用户创建的问卷
        List<PaperVO> paperList=paperMapper.getUserPapers(id);
        for(PaperVO paperVO:paperList){
            //先删除答案，再删除问卷，否则问卷删除之后问卷不存在无法通过问卷ID删除答案
            answerMapper.deleteAnswerByPaperId(paperVO.getId());
            paperService.deletePaper(paperVO.getId());
        }
        userMapper.deleteById(id);
        return ResponseVO.buildSuccess();
    }

}
