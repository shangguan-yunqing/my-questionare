package com.computer.network.serviceImpl;

import com.computer.network.enums.PaperStatus;
import com.computer.network.mapper.AnswerMapper;
import com.computer.network.mapper.PaperMapper;
import com.computer.network.service.AnswerService;
import com.computer.network.vo.AnswerVO;
import com.computer.network.vo.PaperVO;
import com.computer.network.vo.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {
    private final static String EARLY="问卷未开始发放";
    private final static String INVALIDATION="问卷已失效";
    @Autowired
    AnswerMapper answerMapper;
    @Autowired
    PaperMapper paperMapper;

    /**
     * 把问题回答存入数据库
     * @param answerVOList
     * @return
     */
    @Override
    public ResponseVO addAnswers(List<AnswerVO> answerVOList) {
        try {
            int paperId=answerVOList.get(0).getPaperId();//获取第一个答案所属问卷的ID
            PaperVO paperVO=paperMapper.selectByPaperId(paperId);//通过问卷ID获取该问卷
            //判断问卷状态
            if(paperVO.getStartTime()!=null && paperVO.getEndTime()!=null){
                if(paperVO.getStatus()== PaperStatus.INIT)
                    return ResponseVO.buildFailure(EARLY);//问卷未开放，返回失败提示和描述
                if(paperVO.getStatus()== PaperStatus.STOP)
                    return ResponseVO.buildFailure(INVALIDATION);//问卷已失效，返回失败提示和描述
            }
            //问题的回答逐个写入数据库
            for(AnswerVO answerVO:answerVOList)
                answerMapper.addAnswer(answerVO);
            return ResponseVO.buildSuccess();//提交成功
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 根据问卷ID删除该问卷的答案
     * @param paperId
     */
    @Override
    public void deleteAnswerByPaperId(Integer paperId) {
        answerMapper.deleteAnswerByPaperId(paperId);
    }
}


