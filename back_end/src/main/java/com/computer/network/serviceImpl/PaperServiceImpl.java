package com.computer.network.serviceImpl;

import com.computer.network.enums.PaperStatus;
import com.computer.network.mapper.AnswerMapper;
import com.computer.network.mapper.OptionsMapper;
import com.computer.network.mapper.PaperMapper;
import com.computer.network.mapper.QuestionMapper;
import com.computer.network.po.Options;
import com.computer.network.po.Question;
import com.computer.network.service.PaperService;
import com.computer.network.utils.ExportUtils;
import com.computer.network.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PaperServiceImpl implements PaperService {
    private final static String EMPTY="无效ID";
    private final static String USER_EMPTY="用户未创建任何问卷";
    private final static String CSV_CREATE_ERROR="CSV创建失败";
    private final static String XLSX_CREATE_ERROR="XLSX创建失败";
    private final static String UNKNOWN_ERROR="位于PaperServiceImpl的未知错误";
    @Autowired
    PaperMapper paperMapper;
    @Autowired
    QuestionMapper questionMapper;
    @Autowired
    OptionsMapper optionsMapper;
    @Autowired
    AnswerMapper answerMapper;

    /**
     * 添加问卷
     * @param paperVO 问卷
     * @return 调用状态
     */
    @Override
    public ResponseVO addPaper(PaperVO paperVO) {
        try {
            paperMapper.addPaper(paperVO);
            return ResponseVO.buildSuccess(paperVO);
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 更新问卷基本信息
     * @param paperVO 问卷
     * @return 调用状态
     */
    @Override
    public ResponseVO updatePaper(PaperVO paperVO) {
        try {
            PaperVO paper=paperMapper.selectByPaperId(paperVO.getId());//根据传入的问卷ID搜索该问卷
            if(paper==null)//数据库中无该问卷信息
                return ResponseVO.buildFailure(EMPTY);
            else{
                paperMapper.updatePaper(paperVO);//更新数据库中该问卷的信息
                return ResponseVO.buildSuccess();
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 检查问卷信息
     */
    @Scheduled(cron = "0 0/1 * * * ?")    //每分钟检查一次
    void checkPaperStatus(){
        List<PaperVO> paperVOList=paperMapper.getTimePapers();
        for(PaperVO paperVO:paperVOList){
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = sdf.format(date);
            if(paperVO.getStatus()==PaperStatus.INIT){    //没开放的开放它
                if(currentDate.compareTo(paperVO.getStartTime())>=0)
                    paperMapper.changeStatus(PaperStatus.START,paperVO.getId());
            }
            else if(paperVO.getStatus()==PaperStatus.START){   //没结束的结束它
                if(currentDate.compareTo(paperVO.getEndTime())>=0)
                    paperMapper.changeStatus(PaperStatus.STOP,paperVO.getId());
            }
        }
    }

    /**
     * 根据问卷ID删除该问卷所有信息及其关联数据
     * @param paperId 问卷ID
     * @return 调用状态
     */
    @Override
    public ResponseVO deletePaper(int paperId) {
        try {
            PaperVO paper=paperMapper.selectByPaperId(paperId);//根据问卷ID在数据库中搜索该问卷
            if(paper==null)
                return ResponseVO.buildFailure(EMPTY);
            else{
                paperMapper.deletePaper(paperId);//删除问卷基本信息
                //获取该问卷的所有问题
                List<Question> questionList=questionMapper.selectByPaperId(paperId);
                //遍历删除每个问题的选项
                for(Question question:questionList)
                    optionsMapper.deleteByQuestionId(question.getId());
                //根据问卷ID删除该问卷的所有问题
                questionMapper.deleteByPaperId(paperId);
                answerMapper.deleteAnswerByPaperId(paperId);
                return ResponseVO.buildSuccess();
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 根据用户ID获取某个用户创建的全部问卷
     * @param userId 用户ID
     * @return 调用状态（如果成功，则包含该用户创建的问卷列表）
     */
    @Override
    public ResponseVO getUserPapers(int userId) {
        try {
            //根据用户ID在数据库中查找问卷
            List<PaperVO> paperVOList=paperMapper.getUserPapers(userId);
            if(paperVOList==null)
                return ResponseVO.buildFailure(USER_EMPTY);
            else
                return ResponseVO.buildSuccess(paperVOList);
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 根据问卷ID获取问卷详情信息
     * @param paperId 问卷ID
     * @return 调用状态（如果成功，则包含该问卷的详情信息）
     */
    @Override
    public ResponseVO checkPaper(int paperId) {
        try {
            //据问卷ID在数据库中查找问卷
            PaperVO paperVO=paperMapper.selectByPaperId(paperId);
            if(paperVO==null)
                return ResponseVO.buildFailure(EMPTY);
            PaperDetail paperDetail=new PaperDetail();
            BeanUtils.copyProperties(paperVO,paperDetail);//将问卷属性复制到问卷详情上

            List<QuestionVO> questionVOList=new ArrayList<>();
            // 据问卷ID在数据库中查找属于该问卷的所有问题
            List<Question> questionList=questionMapper.selectByPaperId(paperId);
            for(Question question:questionList){
                int questionId=question.getId();
                //根据问题ID在数据库中查找属于该问题的所有选项
                List<Options> optionsList=optionsMapper.selectByQuestionId(questionId);

                QuestionVO questionVO=new QuestionVO();
                //将问题的属性复制到问题详情上
                BeanUtils.copyProperties(question,questionVO);
                //将该问题的选项列表赋给该问题的问题详情
                questionVO.setOptions(optionsList);
                //将该问题详情添加到该试卷的问题详情列表
                questionVOList.add(questionVO);
            }
            //将问题详情列表赋值给问卷详情
            paperDetail.setQuestionList(questionVOList);
            return ResponseVO.buildSuccess(paperDetail);

        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     *根据问卷ID，统计该问卷的回答情况
     * @param paperId 问卷ID
     * @return 调用状态（如果成功，包含该问卷每个问题的回答情况，选择题包含每个选项被选择的次数，简答题包含题目的所有回答
     */
    @Override
    public ResponseVO reviewPaper(int paperId) {
        try {
            //根据问卷ID在数据库中查找该问卷
            PaperVO paperVO=paperMapper.selectByPaperId(paperId);
            if(paperVO==null)
                return ResponseVO.buildFailure(EMPTY);
            else{
                PaperStatistic paperStatistic=new PaperStatistic();
                //把问卷基本信息赋值给问卷统计
                BeanUtils.copyProperties(paperVO,paperStatistic);

                List<QuestionStatistic> questionStatisticList=new ArrayList<>();
                //根据问卷ID在数据库查找该问卷的所有问题
                List<Question> questionList=questionMapper.selectByPaperId(paperId);

                for(Question question:questionList){

                    QuestionStatistic questionStatistic=new QuestionStatistic();
                    //把问题基本信息赋值给问题统计
                    BeanUtils.copyProperties(question,questionStatistic);

                    int questionId=question.getId();

                    if(question.getType()!=3){    //单选题和多选题
                        //根据问题iD在数据库中查找该问题的所有选项
                        List<Options> optionsList=optionsMapper.selectByQuestionId(questionId);

                        List<OptionStatistic> optionStatisticList =new ArrayList<>();

                        for(Options options:optionsList){   //先都转成另一个VO
                            OptionStatistic optionStatistic =new OptionStatistic();
                            //把选项的基本信息赋值给选项统计
                            BeanUtils.copyProperties(options, optionStatistic);
                            //先将该选项统计中的回答次数设为0
                            optionStatistic.setSelectedNum(0);   //后面用于+1
                            //将该选项统计添加到该问题的选项统计列表
                            optionStatisticList.add(optionStatistic);
                        }

                        //根据问题ID在数据库查找该问题的所有回答
                        List<AnswerVO> answerVOList=answerMapper.selectByQuestionId(questionId);

                        for(AnswerVO answerVO:answerVOList){
                            String answerContent=answerVO.getAnswerContent();
                            //分割该回答的内容
                            String[] optionSequenceList=answerContent.split(",");
                            for(String sequenceStr:optionSequenceList){
                                int sequence=Integer.valueOf(sequenceStr);

                                for(OptionStatistic optionStatistic : optionStatisticList){
                                    if(optionStatistic.getSequence()==sequence){
                                        //对应的选项统计中回答次数+1
                                        optionStatistic.setSelectedNum(optionStatistic.getSelectedNum()+1);
                                        break;
                                    }
                                }
                            }
                        }
                        //该问题的选项统计列表赋值给问题统计
                        questionStatistic.setOptionStatistics(optionStatisticList);
                    }
                    else{    //简答题直接将回答添加到回答列表
                        questionStatistic.setAnswerVOList(answerMapper.selectByQuestionId(questionId));
                    }
                    //统计多少人回答了这个问题
                    questionStatistic.setFilledInNum(answerMapper.selectByQuestionId(questionId).size());
                    //将该问题统计添加到该问卷的问题统计列表
                    questionStatisticList.add(questionStatistic);
                }
                //将问题统计列表赋值给问卷统计
                paperStatistic.setQuestionStatistics(questionStatisticList);
                return ResponseVO.buildSuccess(paperStatistic);
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseVO.buildFailure(e.getMessage());
        }
    }

    /**
     * 根据问卷ID导出问卷信息。
     * @param paperId
     * @return
     */
    @Override
    public ResponseVO exportPaper(String type,HttpServletResponse response, int paperId){
        //检查问卷是否存在
        PaperVO paperVO = paperMapper.selectByPaperId(paperId);
        if(paperVO == null){
            return ResponseVO.buildFailure(EMPTY);
        }else{
            //问题列表
            List<Question> questionList = questionMapper.selectByPaperId(paperId);
            //CSV的head前置
            List<String> stringList = new ArrayList<>();
            //CSV的value
            List<String[]> csvValueList = new ArrayList<>();
            //XLSX的value
            List<List<String>> xlsxValueList = new ArrayList<>();

            //对所选问卷的每一个问题处理
            for(Question question:questionList) {
                int questionId = question.getId();
                //每一条value的前置缓冲区
                List<String> cacheList = new ArrayList<>();
                //选择题选项列表
                List<Options> optionsList = optionsMapper.selectByQuestionId(questionId);
                //对应题目的回答列表
                List<AnswerVO> answerVOList = answerMapper.selectByQuestionId(questionId);
                //将问题标题加入
                cacheList.add(question.getTitle());


                if (question.getType() != 3) { //对于选择题
                    for (AnswerVO answerVO : answerVOList) {
                        String str = "";
                        String answerContent = answerVO.getAnswerContent();
                        //分割该回答的内容
                        String[] optionSequenceList = answerContent.split(",");

                        //解析选项对应的文字
                        for (String sequenceStr : optionSequenceList) {
                            int sequence = Integer.valueOf(sequenceStr);
                            //组合成一个字符串作为一次回答
                            str += optionsList.get(sequence-1).getContent()+"|";
                        }
                        //加入缓冲区
                        cacheList.add(str.substring(0,str.length()-1));
                    }
                } else { //对非选择题，直接将回答加入缓冲区
                    for (AnswerVO answerVO : answerVOList) {
                        cacheList.add(answerVO.getAnswerContent());
                    }
                }
                //将每一题的题面和所有回答作为一行加入
                switch (type){
                    case "csv":
                        csvValueList.add(cacheList.toArray(new String[cacheList.size()]));
                        break;
                    case "xlsx":
                        xlsxValueList.add(cacheList);
                        break;
                }

            }

            switch (type) {
                case "csv":
                    //生成表头
                    stringList.add("题目");
                    for(int i=1;i<csvValueList.size()-1;i++){
                        String cacheString = "第"+i+"条回答";
                        stringList.add(cacheString);
                    }
                    String[] head = stringList.toArray(new String[stringList.size()]);
                    try {
                        //生成CSV文件
                        File csv = ExportUtils.makeTempCSV(new String().valueOf(new Date().getTime()), head, csvValueList);
                        System.out.println(csv.toURI().toString());
                        //发送
                        ResponseVO res = ExportUtils.sendFile(response, csv, type);
                        //清理缓存文件
                        csv.deleteOnExit();
                        return res;
                    } catch (IOException e) {
                        return ResponseVO.buildFailure(CSV_CREATE_ERROR);
                    }
                case "xlsx":
                    try {
                        File xlsx = ExportUtils.makeTempXlsx(new String().valueOf(new Date().getTime()),xlsxValueList);
                        System.out.println(xlsx.toURI());
                        ResponseVO res = ExportUtils.sendFile(response, xlsx, type);
                        return res;
                    }catch (IOException e){
                        return ResponseVO.buildFailure(XLSX_CREATE_ERROR);
                    }
                default:
                    return ResponseVO.buildFailure(UNKNOWN_ERROR);
            }
        }
    }

}
