import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/router/permission'


import axios from 'axios'
import VueAxios from 'vue-axios'
import htmlToPdf from '@/utils/htmlToPdf'
Vue.use(htmlToPdf)
Vue.use(VueAxios, axios)

//统一的域
axios.defaults.baseURL = 'http://localhost:8099'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

import { Message } from 'element-ui'

//响应拦截器，根据响应返回信息
axios.interceptors.response.use((response) => {
    switch (response.status) {
        case 200:
            return response
        case 404:
            return false
        case 500:
            Message.error('服务器异常')
            return false
        default:
            Message.error(response.data.message)
            return false
    }
})