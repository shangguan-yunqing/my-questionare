import Cookies from 'js-cookie'

//封装auth.js工具,用vuex实现登录信息校验管理token
const tokenKey = 'WEB-QUESTIONNAIRE-TOKEN'

export function getToken() {
    return Cookies.get(tokenKey)
}

export function setToken(value) {
    return Cookies.set(tokenKey, value)
}

export function removeToken() {
    return Cookies.remove(tokenKey)
}