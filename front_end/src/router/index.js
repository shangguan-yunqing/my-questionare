import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
		//从初始页重定向到登录页
		path: '/',
		name: 'default',
		redirect: '/login'
	},
	{
		//用户填写问卷完成
		path: '/customer/:paperId/complete',
		name: 'complete',
		component: () =>
			import('../views/customer/afterSubmit/AfterSubmit'),
	},
	{
		//发布问卷地址
		path: '/customer/:paperId',
		name: 'customer',
		component: () =>
			import('../views/customer/Customer')
	},
	{
		//登录页
		path: '/login',
		name: 'login',
		component: () =>
			import('../views/Login')
	},
	{
		//问卷相关
		path: '/editor',
		name: 'editor',
		component: () =>
			import('../views/editor/Editor'),
		children: [{
				//问卷总览
				path: 'overview',
				name: 'overview',
				component: () =>
					import('../views/editor/overview/Overview')
			},
			{
				//问卷创建
				path: 'create/:paperId',
				name: 'create',
				component: () =>
					import('../views/editor/create/Create')
			},
			{
				//查看问卷
				path: 'monitor/:paperId',
				name: 'monitor',
				component: () =>
					import('../views/editor/monitor/Monitor')
			},
			{
				//问卷链接分享页
				path: 'paperlink/:paperId',
				name: 'paperlink',
				component: () =>
					import('../views/editor/create/PaperLink')
			},
			{
				//用户列表
				path: 'user',
				name: 'user',
				component: () =>
					import('../views/editor/user/user')
			},
			{
				//用户问卷列表
				path: 'userPaper',
				name: 'userPaper',
				component: () =>
					import('../views/editor/userpaper/userPaper')
			},
			{
				//用户个人中心
				path: 'userpersonal',
				name: 'userpersonal',
				component: () =>
					import('../views/editor/personal/userpersonal')
			}
		]
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
