import router from './index'
import store from '../store'
import { getToken } from '../utils/auth'

//全局前置路由守卫
router.beforeEach((to, from, next) => {
    // console.log(store)
    // console.log(store.state)
    // console.log(store.state.user)

    //重定向，令已登录的用户直接进入程序时直接进入问卷总览页。
    if (store.state.user.loginState) {
        console.log('login state true')
        if (to.path === '/login') {
            next({ name: 'overview' })
        } else {
            next()
        }
        return
    }
    //获取cookies中的token
    const token = getToken()
    console.log(`token: ${token}`)
        //保持vuex状态管理数据与token的同步
    if (token) {
        store.commit('set_loginState', true)
        store.commit('set_userInfo', { id: Number(token) })
        if (to.path === '/login') {
            console.log('go front page')
            next({ name: 'overview' })
        } else {
            next()
        }
        return
    } else if (to.path === '/login' || to.path.startsWith('/customer')) { //没有token只能去登录页或者问卷填写页。
        console.log('in white list')
        next()
    } else {
        console.log('not login and redirect')
        next('/login')
    }
})